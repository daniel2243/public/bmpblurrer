#ifndef BITMAP_H
#define BITMAP_H

#include <filesystem>
#include <fstream>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

// Standard BMP structures 
struct BITMAPFILEHEADER 
{
    uint16_t bfType {0x4D42};
    uint32_t bfSize {0};
    uint16_t bfReserved1 {0};
    uint16_t bfReserved2 {0};
    uint32_t bfOffBits {0};
};

struct BITMAPINFOHEADER
{
    uint32_t biSize {0};
    int32_t biWidth {0};
    int32_t biHeight {0};
    uint16_t biPlanes {1};
    uint16_t biBitCount {0};
    uint32_t biCompression {0};
    uint32_t biSizeImage {0};
    int32_t biXPelsPerMeter {0};
    int32_t biYPelsPerMeter {0};
    uint32_t biClrUsed {0};
    uint32_t biClrImportant {0};
};

struct BITMAPCOLORHEADER
{
    uint32_t bcRed {0x00ff0000};
    uint32_t bcGreen {0x0000ff00};
    uint32_t bcBlue {0x000000ff};
    uint32_t bcAlpha {0xff000000};
    uint32_t bcColorSpace {0x73524742};
    uint32_t bcReserved[16] {0};
};

// Own structures for different types of BMPs
struct COLORTABLE32BIT
{
    std::vector<uint32_t> b8ColorTable {std::vector<uint32_t>(256)};
};

struct PIXELS8BIT
{
    std::vector<std::vector<uint8_t>> p8Pixels{};
};

struct PIXELS32BIT
{
    std::vector<std::vector<uint32_t>> p32Pixels{};
};

struct Bitmap
{
    BITMAPFILEHEADER bmfh;
    BITMAPINFOHEADER bmih;
    BITMAPCOLORHEADER bmch;
    COLORTABLE32BIT ct32;
    PIXELS8BIT pix8;
    PIXELS32BIT px32;

    int sizeofFileHeader {14};

    Bitmap(const std::string& fileName)
    {
        read(fileName);
        writeBlur(fileName);
    }

    void read(const std::string& fileName)
    {
        std::ifstream inputFile{fileName, std::ios::binary};
        if(!inputFile)
        {
            throw std::runtime_error("Error: Can't find file.");
        }

        // Everything is done one by one to make sure the compiler doesn't add padding

        // Reads contents of BITMAPFILEHEADER, and makes sure the file is of correct type
        inputFile.read((char*)&bmfh.bfType, sizeof (bmfh.bfType));
        if(bmfh.bfType != 0x4D42)
        {
            throw std::runtime_error("Error: File type not recognized.");
        }
        inputFile.read((char*)&bmfh.bfSize, sizeof (bmfh.bfSize));
        inputFile.read((char*)&bmfh.bfReserved1, sizeof(bmfh.bfReserved1));
        inputFile.read((char*)&bmfh.bfReserved2, sizeof(bmfh.bfReserved2));
        inputFile.read((char*)&bmfh.bfOffBits, sizeof(bmfh.bfOffBits));

        // Reads contents of BITMAPINFOHEADER, and checks compatibility 
        inputFile.read((char*)&bmih.biSize, sizeof(bmih.biSize));
        if(bmih.biSize != 40)
        {
            throw std::runtime_error("Error: This program only supports files with BITMAPINFOHEADERS of 40 bytes.");
        }
        inputFile.read((char*)&bmih.biWidth, sizeof(bmih.biWidth));
        inputFile.read((char*)&bmih.biHeight, sizeof(bmih.biHeight));
        if(bmih.biHeight < 0)
        {
            throw std::runtime_error("Error: This program only supports files with origin in the bottom left corner.");
        }
        inputFile.read((char*)&bmih.biPlanes, sizeof(bmih.biPlanes));
        if(bmih.biPlanes != 1)
        {
            throw std::runtime_error("Error: Too many color planes.");
        }
        inputFile.read((char*)&bmih.biBitCount, sizeof(bmih.biBitCount));
        if(bmih.biBitCount != 8)
        {
            throw std::runtime_error("Error: This program only supports files with 8 bit pixel size.");
        }
        inputFile.read((char*)&bmih.biCompression, sizeof(bmih.biCompression));
        if(bmih.biCompression != 0 && bmih.biCompression != 3)
        {
            throw std::runtime_error("Error: This program does not support compressed files.");
        }
        inputFile.read((char*)&bmih.biSizeImage, sizeof(bmih.biSizeImage));
        if(bmih.biSizeImage != 0 && bmih.biSizeImage != (bmfh.bfSize - bmfh.bfOffBits))
        {
            throw std::runtime_error("Erro: This program does not support compressed files.");
        }
        inputFile.read((char*)&bmih.biXPelsPerMeter, sizeof(bmih.biXPelsPerMeter));
        inputFile.read((char*)&bmih.biYPelsPerMeter, sizeof(bmih.biYPelsPerMeter));
        inputFile.read((char*)&bmih.biClrUsed, sizeof(bmih.biClrUsed));
        inputFile.read((char*)&bmih.biClrImportant, sizeof(bmih.biClrImportant));

        // Checks pixel size
        if(bmih.biBitCount == 8)
        {
            // Reads Color Table
            for(int i{0}; i<256; ++i)
            {
                inputFile.read((char*)&ct32.b8ColorTable[i], sizeof(ct32.b8ColorTable[8]));
            }

            // Jumps to pixel info, in case there is dead space between Color Table and pixel info
            inputFile.seekg(bmfh.bfOffBits, inputFile.beg);

            // Preps vector for pixel values
            pix8.p8Pixels = std::vector<std::vector<uint8_t>>();
            pix8.p8Pixels.resize(bmih.biHeight);
            for(int i{0}; (size_t) i<pix8.p8Pixels.size(); ++i)
            {
                pix8.p8Pixels[i] = std::vector<uint8_t>{};
                pix8.p8Pixels[i].resize(bmih.biWidth);
            }

            // Gets data for each pixel
            for(int i{0}; i<bmih.biHeight; ++i)
            {
                for(int j{0}; j<bmih.biWidth; ++j)
                {
                    inputFile.read((char*)&pix8.p8Pixels[i][j], sizeof(pix8.p8Pixels[i][j]));
                }
            }
        }
    }

    void writeBlur(const std::string& fileName)
    {
        // Sets up path and name for blurred image
        std::filesystem::path originalImagePath {fileName};
        std::filesystem::path pathToBlurs {std::filesystem::current_path() / "ImageBlurs"};
        std::filesystem::path nameOfBlur {std::filesystem::current_path() / "ImageBlurs" / originalImagePath.stem()};
        nameOfBlur += "Blurred.bmp";
        std::string blurredImage {nameOfBlur.string()};

        
        std::ofstream outputFile{blurredImage, std::ios::binary};
        if(!outputFile)
        {
            throw std::runtime_error("Error: Can't make new file.");
        }
        int originalBitCount {bmih.biBitCount};
        bmfh.bfSize = (sizeofFileHeader + sizeof(bmih) + sizeof(bmch) + (4*((unsigned)(bmih.biWidth))*((unsigned)(bmih.biHeight))));
        bmfh.bfOffBits = (sizeofFileHeader + sizeof(bmih) + sizeof(bmch));
        bmih.biSize = 40;
        bmih.biBitCount = 32;
        bmih.biClrImportant = 0;
        bmih.biClrUsed = 0;
        bmih.biClrImportant = 0;
        writeFileHeader(outputFile);
        writeInfoHeader(outputFile);
        writeColorHeader(outputFile);
        if(originalBitCount != 32)
        {
            translate8To32Bit();
        }
        writeBlurredPixels(outputFile);
    }

    void writeFileHeader(std::ofstream& outputFile)
    {
        outputFile.write((const char*)&bmfh.bfType, sizeof (bmfh.bfType));
        outputFile.write((const char*)&bmfh.bfSize, sizeof (bmfh.bfSize));
        outputFile.write((const char*)&bmfh.bfReserved1, sizeof (bmfh.bfReserved1));
        outputFile.write((const char*)&bmfh.bfReserved2, sizeof (bmfh.bfReserved2));
        outputFile.write((const char*)&bmfh.bfOffBits, sizeof (bmfh.bfOffBits));
    }

    void writeInfoHeader(std::ofstream& outputFile)
    {
        outputFile.write((const char*)&bmih, sizeof (bmih));
    }

    void writeColorHeader(std::ofstream& outputFile)
    {
        outputFile.write((const char*)&bmch, sizeof(bmch));
    }

    void writeBlurredPixels(std::ofstream& outputFile)
    {
        for(int i{0}; i<bmih.biHeight; ++i)
        {
            for(int j{0}; j<bmih.biWidth; ++j)
            {
                std::vector<uint32_t> pixels{};
                unsigned int counter{0};
                if(i-1>=0 && i-1<bmih.biHeight && j-1>=0 && j-1<bmih.biWidth)
                {
                    pixels.push_back(px32.p32Pixels[i-1][j-1]);
                    ++counter;
                }
                if(i-1>=0 && i-1<bmih.biHeight && j>=0 && j<bmih.biWidth)
                {
                    pixels.push_back(px32.p32Pixels[i-1][j]);
                    ++counter;
                }
                if(i-1>=0 && i-1<bmih.biHeight && j+1>=0 && j+1<bmih.biWidth)
                {
                    pixels.push_back(px32.p32Pixels[i-1][j+1]);
                    ++counter;
                }
                if(i>=0 && i<bmih.biHeight && j-1>=0 && j-1<bmih.biWidth)
                {
                    pixels.push_back(px32.p32Pixels[i][j-1]);
                    ++counter;
                }
                if(i>=0 && i<bmih.biHeight && j>=0 && j<bmih.biWidth)
                {
                    pixels.push_back(px32.p32Pixels[i][j]);
                    ++counter;
                }
                if(i-1>=0 && i-1<bmih.biHeight && j+1>=0 && j+1<bmih.biWidth)
                {
                    pixels.push_back(px32.p32Pixels[i][j+1]);
                    ++counter;
                }   
                if(i+1>=0 && i+1<bmih.biHeight && j-1>=0 && j-1<bmih.biWidth)
                {
                    pixels.push_back(px32.p32Pixels[i+1][j-1]);
                    ++counter;
                }
                if(i+1>=0 && i+1<bmih.biHeight && j>=0 && j<bmih.biWidth)
                {
                    pixels.push_back(px32.p32Pixels[i+1][j]);
                    ++counter;
                }
                if(i+1>=0 && i+1<bmih.biHeight && j+1>=0 && j+1<bmih.biWidth)
                {
                    pixels.push_back(px32.p32Pixels[i+1][j+1]);
                    ++counter;
                }
                uint16_t color1 {0};
                uint16_t color2 {0};
                uint16_t color3 {0};
                uint16_t color4 {0};
                uint16_t color5 {0};
                for(unsigned int k{0}; k<=counter; ++k)
                {
                    color1 += (uint8_t)((pixels[k] & bmch.bcAlpha) >> 24);
                    color5 += (uint8_t)((pixels[k] & bmch.bcAlpha) >> 24);
                    color2 += (uint8_t)((pixels[k] & bmch.bcRed) >> 16);
                    color3 += (uint8_t)((pixels[k] & bmch.bcGreen) >> 8);
                    color4 += (uint8_t)(pixels[k] & bmch.bcBlue);
                }
                color1 /= counter;
                color2 /= counter;
                color3 /= counter;
                color4 /= counter;
                uint8_t avgClr1 {(uint8_t)(color1)};
                uint8_t avgClr2 {(uint8_t)(color2)};
                uint8_t avgClr3 {(uint8_t)(color3)};
                uint8_t avgClr4 {(uint8_t)(color4)};
                uint32_t newPixel {(uint32_t(avgClr1) << 24) | (uint32_t(avgClr2) << 16) | (uint32_t(avgClr3) << 8) | (uint32_t(avgClr4))};
                outputFile.write((const char*)&newPixel, sizeof (newPixel));
            }
        }
    }

    void translate8To32Bit()
    {
        px32.p32Pixels = std::vector<std::vector<uint32_t>>();
        px32.p32Pixels.resize(bmih.biHeight);
        for(size_t i{0}; i<px32.p32Pixels.size(); ++i)
        {
            px32.p32Pixels[i] = std::vector<uint32_t>{};
            px32.p32Pixels[i].resize(bmih.biWidth);
        }

        for(int i{0}; i<bmih.biHeight; ++i)
        {
            for(int j{0}; j<bmih.biHeight; ++j)
            {
                uint8_t colorAlpha {(uint8_t)(((ct32.b8ColorTable[pix8.p8Pixels[i][j]]) & bmch.bcAlpha) >> 24)};
                uint8_t colorRed {(uint8_t)(((ct32.b8ColorTable[pix8.p8Pixels[i][j]]) & bmch.bcRed) >> 16)};
                uint8_t colorGreen {(uint8_t)(((ct32.b8ColorTable[pix8.p8Pixels[i][j]]) & bmch.bcGreen) >> 8)};
                uint8_t colorBlue {(uint8_t)((ct32.b8ColorTable[pix8.p8Pixels[i][j]]) & bmch.bcBlue)};
                px32.p32Pixels[i][j] = ((uint32_t(colorAlpha) << 24) | (uint32_t(colorRed) << 16) | (uint32_t(colorGreen) << 8) | (uint32_t(colorBlue)));
            }
        }
    }
};

#endif
