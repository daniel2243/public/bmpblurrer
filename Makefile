CC=g++
CFLAGS := -Wall -Wextra -std=c++17
LDFLAGS :=

# Directories
PROJECT=/home/daniel/code/bmpBlurrer
SOURCES := $(PROJECT)/source
INCLUDE := $(PROJECT)/include
OBJECTS := $(PROJECT)/build

# Source files
SRCS := $(wildcard $(SOURCES)/*.cpp)
OBJS := $(patsubst $(SOURCES)/%.cpp,$(OBJECTS)/%.o,$(SRCS))

# Headers
INCS := -I$(INCLUDE)


TARGET := $(PROJECT)/blur

# Targets
all: $(TARGET)

$(TARGET): $(OBJS)
	@mkdir -p $(OBJECTS)
	$(CC) $(CFLAGS) $(INCS) -o $@ $^ $(LDFLAGS)

$(OBJECTS)/%.o : $(SOURCES)/%.cpp
	@mkdir -p $(OBJECTS)
	$(CC) $(CFLAGS) $(INCS) -c -o $@ $<

.PHONY: all clean

clean:
	rm -rf $(OBJECTS)
	rm -f $(TARGET)

install:
	rm -f $(OBJECTS)
	cp $(TARGET) /bin/
