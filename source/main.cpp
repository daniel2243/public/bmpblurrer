#include "bitmap.h"
#include <cstring>  //maybe unused ###################
#include <filesystem>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

int main()
{
    // Sets up and checks path to images
    std::filesystem::path pathToImages {std::filesystem::current_path() / "ImageSource"};
    if(!std::filesystem::exists(pathToImages))
    {
	    std::string error = std::string("Error can't find path to images (")
		                    + pathToImages.string() + std::string(")");
        throw std::runtime_error (error);
    }

    // Makes a list of available images in folder
    std::vector<std::string> imageList{};
    int count {0};
    std::cout << "\nPlease select image to be blurred from the list below:\n";
    for(const auto& entry : std::filesystem::directory_iterator(pathToImages))
    {
        std::cout << '\n' << count << ": " << entry.path().filename().string();
        imageList.push_back(entry.path().string());
        ++count;
    }

    // Lets user pick which image to blur
    printf("\n\nYour choice: ");
    int selectedFile {-1};
    while(selectedFile<0 || (size_t) selectedFile >= imageList.size())
    {
        std::cin >> selectedFile;
        if (selectedFile<0 || (size_t) selectedFile >= imageList.size())
            printf("\nThat doesn't seem to be in range, please try again: ");
    }
    Bitmap bmp(imageList[selectedFile]);

    return 0;
}
