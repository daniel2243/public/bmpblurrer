Update 1 year later: this is shit and will be remade in pure C.

C++ program to blur a bmp image.

Requires C++ 17 or later.


To run:

- Change project directory in makefile.

- Delete lenaBlurred.bmp in ImageBlurs folder (optional).

- make clean

- ./blur
